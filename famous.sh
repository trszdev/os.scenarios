#!/bin/bash



if [[ $# != 1 ]] || [[ $# == 1 && ( $1 == "-h" || $1 == "--help" ) ]] ; then
    self_name=`basename "$0"`
    echo -e "\t$self_name - print the most famous url in log"
    echo -e "\tif there are several candidates picks one of them"
    exit 1
fi

if [ -f $1 ] ; then
    python -c "
with open('$1', 'rb') as f:
    lines = {}
    for x in (l.split('|')[3] for l in f): 
        if x in lines: lines[x] += 1
        else: lines[x] = 1
    print max(lines, key=lambda x:lines[x])
"
    #cut -d'|' -f4 $1 | sort | uniq -c | sort -r | cut -d' ' -f9 | head -n 1
else
    echo "No such file: $1"
    exit 2
fi

