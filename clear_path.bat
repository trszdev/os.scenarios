@echo off & setlocal enabledelayedexpansion

if "%1"=="/?" (
    echo %~n0 - clears unused directories inside PATH environment variable
    exit 1 /b
)

set new_path=
for %%A in ("%path:;=";"%") do (
    set valid_dir=0
    if exist "%%~A" cd %%~A 2>nul && (
        for %%F in (*.exe) do set valid_dir=1
        for %%F in (*.bat) do set valid_dir=1
        for %%F in (*.com) do set valid_dir=1
        for %%F in (*.cmd) do set valid_dir=1
        if !valid_dir! EQU 1 set new_path=!CD!;!new_path!
    )
)

set new_path=%new_path:~0,-1%
echo %new_path%
setx path "%new_path%" && echo User path was refreshed

endlocal
