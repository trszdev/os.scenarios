@echo off & setlocal

set ICONV=iconv

where %ICONV% >nul 2>&1 || (
    echo     iconv.exe not found. Get it here for free: http://www.mingw.org/
    exit 2 /b
)


if "%1"=="/?" (
    :help
    echo     %~n0 - converts encoding of all .txt files in directory
    echo     usage: %~n0 [input_dir] [input_encoding] [output_dir] [output_encoding]
    echo     example: %~n0 . CP866 out UTF-8
    echo     this will take all .txt files from here and save with same names but in unicode
    echo     errorcodes:
    echo     1 -- help was shown
    echo     2 -- iconv wasnt found
    echo     3 -- invalid syntax
    echo     4 -- unsupported encoding
    echo     5 -- directory not exists
    echo     available encodings:
    %ICONV% -l
    exit 1 /b
)


if 1==2 (
    :invalid_syntax
    echo     invalid syntax. Type "/?" to view help
    exit 3 /b
)

if "%1"=="" goto :invalid_syntax
if "%3"=="" goto :invalid_syntax
if "%2"=="" goto :invalid_syntax
if "%4"=="" goto :invalid_syntax

set curdir=%CD%
cd %1 2>nul || ( echo    "%1" is not a directory. Type "/?" to view help & exit 5 )
cd %3 2>nul || ( echo    "%3" is not a directory. Type "/?" to view help & exit 5 )
cd %curdir%

set inenc=0 & set outenc=0
if 1==2 (
    :unsupported_encoding
    if %outenc% EQU 0 echo     encoding "%4" is not supported. Type "/?" to view help
    if %inenc% EQU 0  echo     encoding "%2" is not supported. Type "/?" to view help
    exit 4 /b
)

for /f "tokens=*" %%e in ('%ICONV% -l') do (
    echo " %%e " | findstr /C:" %4 " >nul && set outenc=1
    echo " %%e " | findstr /C:" %2 " >nul && set inenc=1
)
if %outenc% EQU 0 goto :unsupported_encoding
if %inenc% EQU 0 goto :unsupported_encoding



for %%f in (%1\*.txt) do (
    %ICONV% -f %2 -t %4 "%%~ff" > "%3\%%~nxf" 2>nul
)
:end

endlocal
