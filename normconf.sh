#!/bin/bash


: '
    Время: s, min, h, d
    Расстояние: mm, sm, dm, m, km
    Вес: mg, g, kg, t
    "объем": B, KB, MB, GB, TB
'

if [ -f $1 ] ; then
    IFS=$'\n'
    values=(`cat $1 | cut -d'=' -f2`)
    names=('time' 'distance' 'longtime' 'size' 'weight' 'answer')
    seds=(
        's/s/*1/; s/min/*60/; s/h/*3600/; s/d/*86400/'
        's/mm/*0.001/; s/sm/*0.01/; s/dm/*0.1/; s/km/*1000/; s/m/*1/'
        's/s/*1/; s/min/*60/; s/h/*3600/; s/d/*86400/'
        's/KB/*1/; s/MB/*1024/; s/GB/*1048576/; s/TB/*1073741824/; s/B/*0.0009765625/'
        's/mg/*0.00001/; s/kg/*1/; s/t/*1000/; s/g/*0.001/'
        's/KB/*1/; s/MB/*1024/; s/GB/*1048576/; s/TB/*1073741824/; s/B/*0.0009765625/'
    )
    unset IFS
    for ((i=0; i<6; i++)); do
        parameter=${names[$i]}
        value=${values[$i]}
        expression=`echo $value | sed "${seds[$i]}"`
        #result=`echo $expression | python3 -c "print(eval(input()))"`
        if [[ $expression =~ [^0-9\*\+\-\/] ]] ; then 
            echo "Bad value '`echo $value`' for parameter '$parameter'"
            exit 3
        fi
        result=`awk "BEGIN { print $expression }"`
        echo $parameter=$result
    done
elif [ $1 == '-h' -o $1 == '--help' ] ; then
    echo -e "\ttranslates config units"
    echo -e "\tusage: normconf.sh [file]"
    echo -e "\tor: cat [file] | normconf.sh"
    exit 1
else
    echo "Can't open file $1"
    exit 2
fi
