@echo off & setlocal

if "%1"=="/?" (
    echo    %~n0 - generates help files for system command in current directory
    exit 1 /b
)

FOR /F %%c IN ('help ^| findstr /R "^[A-Z]"') DO (
    %%c /? 1>%%c.txt 2>nul
)

endlocal
