@echo off & setlocal


:: first arg is help; or third is empty
if "%1"=="/?" (
    echo    %~n0 - place a tag in the corner of selected image files
    echo    usage: %~n0 [input_dir] [label] [output_dir]
    exit 1 /b
)


set CONVERT=convert
where %CONVERT% >nul 2>&1 || (
    echo    %CONVERT% not found. Get it here for free: https://www.imagemagick.org/script/binary-releases.php
    echo    tick "install legacy tools"  
    exit 2 /b
)


md %3 2>nul
set initial_dir=%CD%
cd %3 2>nul || (
    echo    %3 is invalid path
    exit 3 /b
)
set output_dir=%CD%
cd %initial_dir%

cd %1 2>nul || (
    echo    %1 is invalid path
    exit 4 /b
)


for /f "tokens=*" %%f in ('dir /b *.png *.jpg *.gif *.bmp *.tga') do (
    %CONVERT% "%%f" -gravity SouthEast -pointsize 25 ^
            -stroke "#000C" -strokewidth 2 -annotate 0 "%2" ^
            -stroke  none   -fill white    -annotate 0 "%2" ^
            "%output_dir%\%%~nf_annotated%%~xf"
)

cd %initial_dir%

endlocal
